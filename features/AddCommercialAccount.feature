Feature: Add Commercial Account
  Adds a commercial account

  Background:
    Given an addCommercialAccountReq consists of:
      | attribute         | validation                                          | type          |
      | name              | required                                            | string        |
      | address           | required                                            | postalAddress |
      | phoneNumber       | required                                            | string        |
      | customerSegmentId | one of (customerSegmentId,customerBrandId) required | number        |
      | customerBrandId   | one of (customerSegmentId,customerBrandId) required | number        |
    And a postalAddress consists of:
      | attribute                 | validation | type   |
      | street                    | required   | string |
      | city                      | required   | string |
      | regionIso31662Code        | required   | string |
      | postalCode                | required   | string |
      | countryIso31661Alpha2Code | required   | string |

  Scenario Outline: Success
    Given I provide an accessToken identifying me as <identity>
    And provide a valid addCommercialAccountReq
    When I execute addCommercialAccount
    Then a commercial account is added to the account-service with attributes:
      | attribute         | value                                     |
      | name              | addCommercialAccountReq.name              |
      | address           | addCommercialAccountReq.address           |
      | phoneNumber       | addCommercialAccountReq.phoneNumber       |
      | customerSegmentId | addCommercialAccountReq.customerSegmentId |
      | customerBrandId   | addCommercialAccountReq.customerBrandId   |

    Examples:
      | identity      |
      | an app        |
      | a partner rep |
