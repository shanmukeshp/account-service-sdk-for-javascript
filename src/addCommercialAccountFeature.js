import {inject} from 'aurelia-dependency-injection';
import AccountServiceSdkConfig from './accountServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AddCommercialAccountReq from './addCommercialAccountReq';

@inject(AccountServiceSdkConfig, HttpClient)
class AddCommercialAccountFeature {

    _config:AccountServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AccountServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Adds a account
     * @param {AddCommercialAccountReq} request
     * @param {string} accessToken
     * @returns {Promise.<string>} id
     */
    execute(request:AddCommercialAccountReq,
            accessToken:string):Promise<string> {

        return this._httpClient
            .createRequest('commercial-accounts')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then((response) => response.content);

    }
}

export default AddCommercialAccountFeature;