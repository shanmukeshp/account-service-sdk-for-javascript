import {inject} from 'aurelia-dependency-injection';
import AccountServiceSdkConfig from './accountServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import CommercialAccountSynopsisView from './commercialAccountSynopsisView';
import SearchCommercialAccountsAssociatedWithPartnerReq from './searchCommercialAccountsAssociatedWithPartnerReq';
import CommercialAccountSynopsisViewFactory from './commercialAccountSynopsisViewFactory';

@inject(AccountServiceSdkConfig, HttpClient)
class SearchCommercialAccountsAssociatedWithCurrentPartnerFeature {

    _config:AccountServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AccountServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Searches the accounts associated with the provided partner SAP account number
     * by partial account name match
     * @param {SearchCommercialAccountsAssociatedWithPartnerReq} request
     * @param accessToken
     * @returns {Promise.<CommercialAccountSynopsisView[]>}
     */
    execute(request:SearchCommercialAccountsAssociatedWithPartnerReq,
            accessToken:string):Promise<Array> {

        return this._httpClient
            .createRequest(`commercial-accounts`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams(request.toJSON())
            .send()
            .then(response =>
                Array.from(
                    response.content,
                    contentItem =>
                        CommercialAccountSynopsisViewFactory.construct(contentItem)
                )
            );
    }
}

export default SearchCommercialAccountsAssociatedWithCurrentPartnerFeature;