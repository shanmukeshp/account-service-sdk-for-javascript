import {PostalAddress} from 'postal-object-model';

/**
 * @class {AddCommercialAccountReq}
 */
export default class AddCommercialAccountReq {

    _name:string;

    _address:PostalAddress;

    _phoneNumber:string;

    /*_customerSegmentId:number;

    _customerBrandId:number;*/

    _customerType:string;

    _customerSubType:string;

    _customerSubSubType:string;

    _customerBrand:string;

    _customerSubBrand:string;

    /**
     *
     * @param name
     * @param address
     * @param phoneNumber
     * @param customerType
     * @param customerSubType
     * @param customerSubSubType
     * @param customerBrand
     * @param customerSubBrand
     */
    constructor(name:string,
                address:PostalAddress,
                phoneNumber:string,
                //customerSegmentId:number = null,
                //customerBrandId:number = null) {
                customerType:string,
                customerSubType:string,
                customerSubSubType:string,
                customerBrand:string,
                customerSubBrand:string){


        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        if (!address) {
            throw new TypeError('address required');
        }
        this._address = address;

        if (!phoneNumber) {
            throw new TypeError('phoneNumber required');
        }
        this._phoneNumber = phoneNumber;

        /*if (!customerSegmentId && !customerBrandId) {
            throw new TypeError('customerSegmentId or customerBrandId required');
        }
        if (customerSegmentId && customerBrandId) {
            throw new TypeError('customerSegmentId and customerBrandId cannot both be specified');
        }
        this._customerSegmentId = customerSegmentId;
        this._customerBrandId = customerBrandId;*/

        if(!customerType){
            throw new TypeError('customerType required');
        }
        this._customerType = customerType;

        if(!customerSubType){
            throw new TypeError('customerSubType required');
        }
        this._customerSubType = customerSubType;

        this._customerSubSubType = customerSubSubType;

        this._customerBrand = customerBrand;

        this._customerSubBrand = customerSubBrand;
    }


    /**
     * @returns {string}
     */
    get name():string {
        return this._name;
    }

    /**
     * @returns {PostalAddress}
     */
    get address():PostalAddress {
        return this._address;
    }

    /**
     * @returns {string}
     */
    get phoneNumber():string {
        return this._phoneNumber;
    }

    /*/!**
     * @returns {number}
     *!/
    get customerSegmentId():number {
        return this._customerSegmentId;
    }

    /!**
     * @returns {number}
     *!/
    get customerBrandId():number {
        return this._customerBrandId;
    }*/


    get customerType():string{
        return this._customerType;
    }

    get customerSubType():string{
        return this._customerSubType;
    }

    get customerSubSubType():string{
        return this._customerSubSubType;
    }

    get customerBrand():string{
        return this._customerBrand;
    }

    get customerSubBrand():string{
        return this._customerSubBrand;
    }

    toJSON() {
        return {
            name: this._name,
            address: this._address,
            phoneNumber: this._phoneNumber,
            //customerSegmentId: this._customerSegmentId,
            //customerBrandId: this._customerBrandId
            customerType:this._customerType,
            customerSubType:this._customerSubType,
            customerSubSubType:this._customerSubSubType,
            customerBrand:this._customerBrand,
            customerSubBrand:this._customerSubBrand
        };
    }

}
