import SearchCommercialAccountsAssociatedWithPartnerReq from '../../src/searchCommercialAccountsAssociatedWithPartnerReq';
import dummy from '../dummy';

/*
 test methods
 */
describe('SearchCommercialAccountsAssociatedWithPartnerReq class', () => {
    describe('constructor', () => {
        it('throws if partialName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new SearchCommercialAccountsAssociatedWithPartnerReq(
                        null,
                        dummy.sapAccountNumber
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partialName required');

        });
        it('sets partialName', () => {
            /*
             arrange
             */
            const expectedPartialName = dummy.name;

            /*
             act
             */
            const objectUnderTest =
                new SearchCommercialAccountsAssociatedWithPartnerReq(
                    expectedPartialName,
                    dummy.sapAccountNumber
                );

            /*
             assert
             */
            const actualPartialName =
                objectUnderTest.partialName;

            expect(actualPartialName).toEqual(expectedPartialName);

        });
        it('throws if partnerAccountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new SearchCommercialAccountsAssociatedWithPartnerReq(
                        dummy.name,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerAccountId required');

        });
        it('sets partnerAccountId', () => {
            /*
             arrange
             */
            const expectedPartnerAccountId = dummy.sapAccountNumber;

            /*
             act
             */
            const objectUnderTest =
                new SearchCommercialAccountsAssociatedWithPartnerReq(
                    dummy.name,
                    expectedPartnerAccountId
                );

            /*
             assert
             */
            const actualPartnerAccountId =
                objectUnderTest.partnerAccountId;

            expect(actualPartnerAccountId).toEqual(expectedPartnerAccountId);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new SearchCommercialAccountsAssociatedWithPartnerReq(
                    dummy.name,
                    dummy.sapAccountNumber
                );

            const expectedObject =
            {
                partialName: objectUnderTest.partialName,
                partnerAccountId: objectUnderTest.partnerAccountId
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
