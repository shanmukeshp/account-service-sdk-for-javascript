import {PostalAddress} from 'postal-object-model';

const dummy = {
    name: 'facilityName',
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '123131233',
    streetAddress: 'street',
    city: 'city',
    iso31662Code: 'WA',
    postalCode: 'postalCode',
    iso31661Alpha2Code: 'US',
    id:'001K000001HP8SOIA1',
    customerType:'Clubs',
    customerSubType:'Premium',
    customerSubSubType:'New',
    customerBrand:'Energy',
    customerSubBrand:'Energy',
    accountId: '000000000000000000',
    sapAccountNumber: 'sapAccountNo',
    sapVendorNumber: '0000000000',
    userId: 'email@test.com',
    url: 'https://dummy-url.com'
};

dummy.address =
    new PostalAddress(
        dummy.streetAddress,
        dummy.city,
        dummy.iso31662Code,
        dummy.postalCode,
        dummy.iso31661Alpha2Code
    );

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default dummy;